import { userResolver, userQuery } from './userResolver'
import { authMutation, authQuery } from './authResolver'
import { workspaceResolver, workspaceQuery, workspaceMutation } from './workspaceResolver'

const resolvers = {
    Workspace: workspaceResolver,
    User: userResolver,
    Query: {
        ...userQuery,
        ...authQuery,
        ...workspaceQuery,
    },
    Mutation: {
        ...authMutation,
        ...workspaceMutation,
    }
}
export default resolvers
