import { signIn, signUp } from '../../services/authService'

export const authResolver = {}

export const authQuery = {
    async signIn(_, { email, password }) { return await signIn(email, password) },
}

export const authMutation = {
    async signUp(_, { email, password }) { return await signUp(email, password) },
}
