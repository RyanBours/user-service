import { fetchUserById, fetchUsers } from '../../services/userService'

export const userResolver = {
    async __resolveReference(user) { return await fetchUserById(user.id) }
}

export const userQuery = {
    async user() { return await fetchUsers() },
    async userById(_, { id }) { return await fetchUserById(id) },
}

export const userMutation = {}
