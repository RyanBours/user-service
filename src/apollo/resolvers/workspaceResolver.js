import { ForbiddenError, AuthenticationError } from 'apollo-server-errors'
import { Workspace, User } from '../../models'

export const workspaceResolver = {
    __resolveReference(object) {
        return Workspace.findById(object.id)
    },
    async owner(parent) {
        return await User.findOne({ id: parent.owner.toString() })
    },
    async collaborators({collaborators}) {
        const result = await User.find({'_id': {$in: collaborators}})
        return result
    }
}

export const workspaceQuery = {
    async workspace(parent, args, context) {
        const { id:_id, owner, collaborators } = args
        return await Workspace.find({
            ...(!_id || {_id}),
            ...(!owner || {owner}),
            ...(!collaborators || {collaborators: {$in: collaborators}})
        })
    }
}

export const workspaceMutation = {
    async createEmptyWorkspace(parent, args, context) {
        // TODO: move logic to repository
        const { userId } = context

        if (!userId || userId === 'undefined') {
            return new AuthenticationError('Not authenticated')
        }

        const workspace = new Workspace()
        workspace.title = 'New Workspace'
        workspace.owner = userId
        workspace.collaborators = [userId]
        
        await workspace.save()

        return workspace
    }
}
