import { gql } from 'apollo-server-core'

const typeDefs = gql`
    type Mutation {
        signUp(email: String, password: String): User
        createEmptyWorkspace: Workspace
    }
    type Query {
        user: [User]
        userById(id: String): User
        workspace(id: String, owner: String, collaborators: [String]): [Workspace]
        signIn(email: String, password: String): User
    }
    type User @key(fields: "id") {
        id: ID!
        email: String!
        createdAt: String! # TODO: datetime custom scalar
        updatedAt: String!
    }
    type Workspace @key(fields: "id") {
        id: ID!
        title: String!
        collaborators: [User]
        owner: User
        public: Boolean!
        createdAt: String!
        updatedAt: String!
    }
`

export default typeDefs
