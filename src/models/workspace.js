import { model, Schema } from 'mongoose'

const workspaceSchema = new Schema({
    title: {
        type: String,
        require: true,
    },
    collaborators: [{
        type: Schema.Types.ObjectId,
        ref: 'User',
    }],
    public: {
        type: Boolean,
        default: false,
    },
    owner: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        require: true,
    },
}, {
    timestamps: {}
})

const Workspace = model('workspace', workspaceSchema)

export default Workspace
