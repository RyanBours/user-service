require('dotenv').config()
import { ApolloServer } from 'apollo-server-express'
import { ApolloServerPluginDrainHttpServer } from 'apollo-server-core'
import express from 'express'
import http from 'http'
import { buildSubgraphSchema } from '@apollo/federation'
import mongoose from 'mongoose'

import resolvers from './apollo/resolvers'
import typeDefs from './apollo/typedefs'

const PORT = process.env.PORT || 8000

const mongooseConfig = {
  useNewUrlParser: true,
  dbName: process.env.DB_NAME || 'user',
  // user: process.env.DB_USER,
  // pass: process.env.DB_PASS,
  useUnifiedTopology: true
}

const startGqlServer = async () => {
  const app = express()
  const httpServer = http.createServer(app)

  const server = new ApolloServer({
    schema: buildSubgraphSchema([{ typeDefs, resolvers }]),
    plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
    context: ({ req }) => {
      return {
        userId: req.headers['user-id']
      }
    }
  })

  await server.start()

  server.applyMiddleware({ app, path: '/' })
  console.log(PORT)
  await new Promise(resolve => httpServer.listen({ port: PORT }, resolve))
  console.log(`🚀 Server ready at http://localhost:${PORT}${server.graphqlPath}`)
}

const startServer = async () => {
  await mongoose
    .connect(process.env.DB_URL || 'mongodb://host.docker.internal:27017/', mongooseConfig)
    .then(() => {
      console.log('Connected to MongoDB')
      startGqlServer()
    })
}

console.log('starting server')
startServer()
  .catch(err => console.error(err))
