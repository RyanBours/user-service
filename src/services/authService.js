import bcrypt from 'bcrypt'

import { User } from '../models/index'

export const signUp = async (email, password) => {
  const saltRounds = 10
  
  const hash = await bcrypt.hash(password, saltRounds).catch(e => {throw e})

  const user = new User({ email: email, password: hash })
  
  const result = await user.save().catch(e => {throw e})
  if (result) {
    return user
  }
}

export const signIn = async (email, password) => {
  const user = await User.findOne({ email: email }).catch(e => {throw e})
  if (!user) {
    return null
  }

  const compare = await bcrypt.compare(password, user.password).catch(e => {throw e})
  if (!compare) {
    return null
  }

  return user
}
