import { User } from '../models'

export const fetchUsers = async () => {
    const users = await User.find()
        .then(users => users)
        .catch(e => { throw e })
    return users
}

export const fetchUserById = async (id) => {
    const user = await User.findById(id)
        .then(user => user)
        .catch(e => { throw e })
    return user
}

export const fetchUserByEmail = async (email) => {
    const user = await User.findOne({ email: email })
        .then(user => user)
        .catch(e => { throw e })
    return user
}

export const createUser = async (email, password) => {
    const user = new User({ email: email, password: password })
    const result = await user.save()
    if (result === user) {
        return user
    } else {
        // TODO: handle error
        throw new Error('Error creating user')
    }
}

export const deleteUser = async (id) => {
    const user = await User.findByIdAndDelete(id)
        .then(user => user)
        .catch(e => { throw e })
    return user
}
