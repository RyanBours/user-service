FROM node:12 AS base
WORKDIR /src

COPY package*.json yarn.lock ./
RUN yarn install --production
COPY . .
RUN yarn build

ENV PORT=8000

CMD [ "yarn", "prod" ]