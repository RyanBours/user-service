import { MongoClient } from 'mongodb'
import { MongoMemoryServer } from 'mongodb-memory-server'
import { User } from '../src/models'
import mongoose from 'mongoose'
import { createUser, fetchUserByEmail, fetchUserById, fetchUsers } from '../src/services/userService'

describe('User Service test', () => {
    let con
    let mongoServer

    beforeAll(async () => {
        mongoServer = await MongoMemoryServer.create()
        con = await MongoClient.connect(mongoServer.getUri(), {})
        await mongoose.connect(mongoServer.getUri(), { dbName: 'user-service' })
    })

    afterAll(async () => {
        if (con) {
            await con.close()
        }
        if (mongoServer) {
            await mongoServer.stop()
        }
        if (mongoose) {
            await mongoose.disconnect()
        }
    })

    beforeEach(async () => {
        await createUser(
            'john1@doe.test',
            '123456'
        )
        await createUser(
            'john2@doe.test',
            '123456'
        )
        await createUser(
            'john3@doe.test',
            '123456'
        )
    })

    afterEach(async () => {
        await User.deleteMany({})
    })

    it('should create a user', async () => {
        await createUser(
            'john0@doe.test',
            '123456'
        )

        const users = await User.find()

        expect(users.length).toBe(4)
    })

    it('should fail creating a user on duplicate email', async () => {
        let error
        try {
            await createUser(
                'john1@doe.test',
                '123456'
            )
        } catch (e) {
            error = e
        }

        const users = await User.find()

        expect(users.length).toBe(3)
        expect(error.code).toBe(11000)
    })

    it('find all', async () => {
        const users = await fetchUsers()
        expect(users.length).toBe(3)
    })

    it('find by id', async () => {
        const users = await fetchUsers()
        const target_user = users[0]

        const user = await fetchUserById(target_user.id)

        expect(user.id).toBe(target_user.id)
        expect(user.email).toBe(target_user.email)
    })

    it('find by email', async () => {
        const users = await fetchUsers()
        const target_user = users[0]

        const user = await fetchUserByEmail(target_user.email)

        expect(user.id).toBe(target_user.id)
        expect(user.email).toBe(target_user.email)
    })

})
